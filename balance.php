<?php

function balance($left, $right)
{
  $balance = (preg_match_all('/\!/', $left)*2 + preg_match_all('/\?/', $left)*3) - (preg_match_all('/\!/', $right)*2 + preg_match_all('/\?/', $right)*3);
  if ($balance < 0) {
    return 'RIGHT';
  }elseif($balance > 0){
    return 'LEFT';
  }

  return 'BALANCE';
}
